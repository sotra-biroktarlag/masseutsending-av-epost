# Masseutsending av fakturaer

Skal man sende ut faktura til mange mennesker er det styr å lage de manuelt, men det finnes en enkel måte å gjøre dette med programmer man gjerne har installert allerede og ikke koster noen kroner.

Ved å ha ett regneark med navn, adresse og epostadresser og et tekstdokument med fakturateksten, variable navn og adresser kan man lage tilpassede fakturaer/brev til alle medlemmene og sende ut med LibreOffice.

Man trenger [LibreOffice](https://www.libreoffice.org/download/download/) Writer og Calc installert.

## Framgangsmåte

Framgangsmåten tar utgangspunkt i filene i [eksempelfiler](./eksempelfiler)

1. Åpne `faktura_sertifiseringskurs_2019.odt` og tilpass teksten
2. Koble regnearket til dokumentet som en database `Edit - Exchange Database ...`
   - Klikk Browse og finn regnearket
   - Under `Available Databases` i fanen til høyre, klikk på `Form Responses 1` eller hva fanen i regnearket heter.
   - Klikk `Define`
3. Hvis man klikker på `View`og haker av `Data Sources` får man en oversikt på toppen over alle feltene i regnearket. Her kan man trykke og dra ned øverste linje i tabellen og sitte inn nye variabler
4. Sørg for at feltene med navn og variablene i teksten heter det samme ellers vil det ikke virke
5. Sett opp epostinnstillingene i Writer så du kan sende epost
6. Fra menyen på toppen klikk på `Tools` og `MailMerge Wizard`
   - `Use the current document` og `Next`
   - `E-mail message` og `Next`
   - Klikk på `Select Address List...` og sørg for at regnearket ditt er valgt. Klikk `Next`
   - Klikk forbi `Create Salutation-siden`, det trenger vi ikke, ved å klikke på `Finish`
7. Nå har det kommet en ny verktøyslinje med noen ioner med noen dokumenter og blå piler og konvelutter, her kan man klikke på
   - `Save Merged Documents` for å se alle fakturaene som blir generert. Her kan man generere PDFer/dokumenter for alle fakturaene og sende de manuelt fra en annen eposttjener.
   - `Send E-mail messages` for å faktisk sende fakturaene direkte fra Writer
   - `Edit individual documents` kan være greit for enkelt å se at de ser OK ut
8. Klikk på `Send E-mail messages` og under `E-mail options` sørg for at `To`-feltet er satt til `Epost`-fanen i regnearket så de faktiske epostadressene blir valgt. Velg et `Subjekt` som blir tittelen på eposten og velg formatet på eposten i `Send as` menyen. HTML fungerer helt fint.
9. Klikk på `Send Documents` og du er ferdig.

Se [LibreOffice4mailmerge.pdf](./LibreOffice4mailmerge.pdf) for en alternativ / den orginale framgangsmåten.

## Bytte navn på alle filene

for masseendringer av navnene på de genererte dokumentene kan kommandoen under kjøres i terminalen. Bytt ut teksten `støttemedlem2019` med hva enn du ønsker å kalle filene.

Dette er nyttig når man må lagre bilag.

```bash
find -name '*.odt' | # find odts
gawk 'BEGIN{ a=1  }{ printf "mv %s bilag_%d_støttemedlem2019.odt\n", $0, a++  }' | # build mv command
bash # run that command
```

## Konvertere alle ODT til PDF

For å konvertere alle dokumentene til PDF kan følgende kommando kjøres i terminalen

```bash
sudo apt-get install unoconv
unoconv -f pdf *.odt
```

## Alternativer til å sende med LibreOffice

- [Conta faktura](https://conta.no/)
- Bruker man [Fiken](https://fiken.no/) kan man også sende fakturaer der
